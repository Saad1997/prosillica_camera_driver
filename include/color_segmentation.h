/**
 * @file color_segmentation.h

 * @author Saad Abdullah (saad1@uni-bremen.com)
 * @brief Implementation of frame acquisition from Prosillica Camera and their brightness control  
 * @version 0.1
 * @date 2022-08-17
 * 
 * 
 */


#define _x64 1
#define _LINUX 1


#include <stdio.h>
#include <Eigen/Geometry>
#include <opencv2/core/eigen.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include <opencv2/aruco.hpp>
#include <string.h>
#include <iostream>
#include <time.h> 
#include "prosilica_gige_sdk/PvApi.h"

using namespace std;

using namespace cv;

enum Color{ //object tracking using these colors
        BLUE, GREEN ,RED ,YELLOW, BLACK,
        };


class Prosillica_Driver{
    
    public:
       
        cv::Mat image , color , imagen , image__ , ip_img; //original grayscale image carrying Camera.Frame 
        
        std::vector<int> ids;
        std::vector<std::vector<cv::Point2f> >  corners;
        
        typedef struct{
            unsigned long UID;
            tPvHandle Handle;
            tPvFrame Frame; } tCamera;
         
         //Prosillica_Driver() = default;
         Prosillica_Driver(tCamera* current_cam, const int max_capture_width, const int max_capture_height, int desired_width, int desired_height);
         void brightness_control(cv::Mat * image ,cv::Mat * op_img, int brightness , double contrast);
         void marker_detection(cv::Mat * inpImage );
         void color_detc(cv::Mat* input_img  ,  cv::Mat * output_img ,Color col);
         //TODO
         //std::vector<int> *  marker_position(std::vector<cv::Point2f> * marker_cor ); //provide marker position in camera
         double marker_distance( float fx , float fy , std::vector<int> * image_size , cv::Size2f marker_size); //provide distance of marker from camera
         void euler2Quaternion(double angle , double  distance_from_camera, cv::Point2f * img_coor , Eigen::Matrix3d &Camera_Mat);  
         cv::Mat * get_AUV_position( Eigen::Matrix3f & camera_mat , Eigen::MatrixXf & dist_mat , std::vector<Point2f> & cont , cv::Point2f & tag_center, double depth);


         
        ~Prosillica_Driver(){ delete this;};

        

    private:

   
        
    


};



