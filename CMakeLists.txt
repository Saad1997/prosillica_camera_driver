#Basic Starting point

cmake_minimum_required(VERSION 3.10)
project(auv_detect VERSION 1.2)
#specify C++ stantdards
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

###############################find pakcages and headerfiles##############################################
find_package( OpenCV REQUIRED )
find_package(Eigen3 3.3 REQUIRED)
find_package( prosilica_gige_sdk REQUIRED )
include_directories( ${PROJECT_SOURCE_DIR}/include ${OpenCV_INCLUDE_DIRS} "/usr/local/include/prosilica_gige_sdk/PvAPI.h")
##################################################################


########################################################creating library######################
add_library(auv SHARED src/color_segmentation.cpp)

target_link_libraries(auv  ${OpenCV_LIBS} "/usr/local/lib/libPvAPI.so"  Eigen3::Eigen)
target_include_directories( auv PUBLIC ${PROJECT_SOURCE_DIR}/include ${Eigen3_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS} "/usr/local/include/prosilica_gige_sdk/PvAPI.h")
################################################################################### 


########################################### add executables############################
add_executable(auv_extraction src/auv_detection.cpp)
target_include_directories(auv_extraction PUBLIC  ${Eigen3_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS} ${} auv "/usr/local/include/prosilica_gige_sdk/PvAPI.h")
target_link_libraries(auv_extraction PUBLIC Eigen3::Eigen ${OpenCV_LIBS} auv "/usr/local/lib/libPvAPI.so") ##EXTRA_LIBS will link any optional liberaries to later be linked in to exectueable

