
#include <color_segmentation.h>
#include <iterator>



static bool readCameraParameters(const char*filename,Eigen::Matrix3f &camMatrix, Eigen::MatrixXf &distCoeffs) {
            FileStorage fs(filename, FileStorage::READ);
            if(!fs.isOpened())
                return false;
            cv::Mat cam , dist; 
            fs["K"] >> cam;
            fs["D"] >> dist;
            cv2eigen(cam, camMatrix);
            cv2eigen(dist, distCoeffs);
            fs.release();
            return true;
}













int desired_width = 640 ;
int desired_height = 480 ;
int max_capture_width = 2434;
int max_capture_height = 2024;
double alpha = 5.0; /*< Simple contrast control */
int beta = 50;       /*< Simple brightness control */
double depth = 0.0;
Eigen::Matrix3f cameraMatrix;
Eigen::MatrixXf dist;
Eigen::Matrix3f rotation_matrix;
std::vector<cv::Vec3d> rvecs , tvecs = {};
std::vector<int> img_size = {};
const char* filename = "/home/saad/Desktop/DFKI/Cameras/Camera_Callibration/calibration.yml";



int main(int argc, char **argv){

if (argc < 4){
     std::cout <<"prosillica_driver_test desird_width desired_height detection_mode " <<std::endl;
    std::cout << "detection_mode can be \"Color_Detc\" or \"Tag_Detc\" " <<std::endl;
    std::cout<<"Example:  ./auv_extraction 640 480 Tag_Detc "<< std::endl;

    exit(1);
}
if((strcmp(argv[1] ,  "--help") == 0)){
    std::cout <<"prosillica_driver_test desird_width desired_height detection_mode " <<std::endl;
    std::cout << "detection_mode can be \"Color_Detc\" or \"Tag_Detc\" " <<std::endl;
    std::cout<<"Example:  ./auv_extraction 640 480 Tag_Detc "<< std::endl;

    exit(1);
}
else{
desired_width = atoi(argv[1]);
desired_height = atoi(argv[2]);
for(int i = 0; i< img_size.size(); i++){
        img_size[0]= desired_height;
        img_size[1] = desired_width;
        }
}



if (argc < 4){
    std::cout << "Error: prosillica_driver_test desird_width desired_height detection_mode" <<std::endl;
    std::cout << "detection_mode can be \"Color_Detc\" or \"Tag_Detc\" " <<std::endl;
    exit(1);
}

readCameraParameters(filename , cameraMatrix , dist);
Prosillica_Driver::tCamera* myCamera; 
Prosillica_Driver* camera = new Prosillica_Driver(myCamera,max_capture_width,max_capture_height, desired_width, desired_height);

            std::cout<<"hello"<<std::endl;


  for (;;)
    {
        
        if (!PvCaptureQueueFrame(myCamera->Handle, &(myCamera->Frame), NULL))
        {
            while (PvCaptureWaitForFrameDone(myCamera->Handle, &(myCamera->Frame), 100) == ePvErrTimeout)
            {
            }
            camera->image.data = (uchar *)myCamera->Frame.ImageBuffer;
            
            cvtColor(camera->image, camera->color, cv::COLOR_BayerRGGB2BGR) ;//cv::COLOR_BayerRGGB2BGR) ;
             
            camera->brightness_control(&camera->color, &camera->imagen , beta , alpha);//control brightness
           
            camera->marker_detection(&camera->imagen);
            
            if(camera->ids.size() > 0){
              cv::RotatedRect bbox =  cv::minAreaRect(  camera->corners.at(0));
              depth = camera->marker_distance( cameraMatrix.coeff(0, 0) ,  cameraMatrix.coeff(1,1) ,&img_size , bbox.size ); //provide distance of marker from camera
              //  std::cout<<"distance from camera in cm"<<std::endl;
              //  std::cout<<depth<<std::endl;
               // std::cout<<"angle"<<std::endl;
              //  std::cout<<bbox.angle<<std::endl;
               //camera->euler2Quaternion(double(bbox.angle) , pixels_mm ,&bbox.center , cameraMatrix  );  

              static cv::Mat * loc =  camera->get_AUV_position( cameraMatrix , dist ,   camera->corners.at(0), bbox.center, depth );
              float x =( (23.5) + loc->at<float>(0) )/2;
              float y = ((-8) + abs(loc->at<float>(1)))/ 2; 
                //std::cout << *loc <<std::endl;
              printf(" x : %f , y: %f \n " , x , y);

               }

            if(strcmp( argv[3] , "Color_Detc")==0)
            {
                camera->color_detc(&camera->color, &camera->ip_img , Color::YELLOW);
                
                imshow("color",camera->ip_img);
                cv::waitKey(25);  

                
            }
            else if(strcmp( argv[3] ,  "Tag_Detc") == 0){    

                imshow("tag",camera->imagen); 
                cv::waitKey(25);  
               
                
            }
            


        }
    }
    return 0;
}









