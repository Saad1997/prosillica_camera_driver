
#include "color_segmentation.h"



//using namespace cv;
//using namespace prosillica_gx;
Prosillica_Driver::Prosillica_Driver(tCamera* current_cam,const int max_capture_width,const int max_capture_height, int desired_width, int desired_height){
    this->image = cv::Mat(desired_height ,desired_width , CV_8UC1); 
    this->color = cv::Mat(desired_height ,desired_width , CV_8UC3); 
    this->imagen = cv::Mat(desired_height , desired_width ,CV_8UC3); 
    this->ip_img =  cv::Mat(desired_height , desired_width ,CV_8UC3);

    
     // Initialize the PvAPI interface so that we can look for cameras
    if (!PvInitialize())
    {
        // Wait for the response from a camera after the initialization of the driver
        // This is done by checking if camera's are found yet
        while (PvCameraCount() == 0)
        {
            cv::waitKey(15);
        }

        // If there is a camera connected to the camera 1 interface, grab it!
        tPvCameraInfo cameraInfo;
        if (PvCameraList(&cameraInfo, 1, NULL) == 1)
        {
            unsigned long frameSize;

            // Get the camera ID
            current_cam->UID = cameraInfo.UniqueId;
            // Open the camera
            if (!PvCameraOpen(current_cam->UID, ePvAccessMaster, &(current_cam->Handle)))
            {
                // Debug
                cout << "Camera opened succesfully" << endl;

                // Get the image size of every capture
                PvAttrUint32Get(current_cam->Handle, "TotalBytesPerFrame", &frameSize);

                // Allocate a buffer to store the image
                memset(&current_cam->Frame, 0, sizeof(tPvFrame));
                current_cam->Frame.ImageBufferSize = frameSize;
                current_cam->Frame.ImageBuffer = new char[frameSize];

                // Set maximum camera parameters - camera specific
                // Code will generate an input window from the center with the size you want
                int center_x = max_capture_width / 2;
                int center_y = max_capture_height / 2;

                // Set the manta camera parameters to get wanted frame size retrieved
                PvAttrUint32Set(current_cam->Handle, "RegionX", center_x - (desired_width / 2));
                PvAttrUint32Set(current_cam->Handle, "RegionY",center_y - (desired_height / 2));
                PvAttrUint32Set(current_cam->Handle, "Width", desired_width);
                PvAttrUint32Set(current_cam->Handle, "Height", desired_height);

                // Start the camera
                PvCaptureStart(current_cam->Handle);

                // Set the camera to capture continuously
                PvAttrEnumSet(current_cam->Handle, "AcquisitionMode", "Continuous");
                PvCommandRun(current_cam->Handle, "AcquisitionStart");
                PvAttrEnumSet(current_cam->Handle, "FrameStartTriggerMode", "Freerun");
            }
            else
            {
                cout << "Opening camera error" << endl;
            }
        }
        else
        {
            cout << "Camera not found or opened unsuccesfully" << endl;
        }
    }
    else
    {
        // State that we did not succeed in initializing the API
        cout << "Failed to initialise the camera API" << endl;
    }
    
    
    
}


void Prosillica_Driver::brightness_control(cv::Mat * image, cv::Mat* op_img , int brightness , double contrast){

cv::Mat new_image =  Mat::zeros( image->size(), image->type() );
for( int y = 0; y < image->rows; y++ ) {
                for( int x = 0; x < image->cols; x++ ) {
                    for( int c = 0; c < image->channels(); c++ ) {
                        op_img->at<Vec3b>(y,x)[c] = saturate_cast<uchar>( contrast*image->at<Vec3b>(y,x)[c] + brightness );
                    }
                }
            }

}


void Prosillica_Driver::marker_detection(cv::Mat * inpImage){


      cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_APRILTAG_36h11);
      cv::aruco::detectMarkers(*inpImage, dictionary, corners, ids);

      if (ids.size() > 0){
        cv::aruco::drawDetectedMarkers(*inpImage, corners, ids);
      }

}


void Prosillica_Driver::color_detc(cv::Mat * input_img , cv::Mat * output_img , Color col){
          
/**
           * @brief  Takes an input image , provides an output image with bounding box on segmented instance as well as center location of bounding box 
           * Arguments:
           * input_img: input image 
           * output_img: output_image
           * Color col : Color of object to segment , from Color enum , Color::Blue |Color::Green|Color::RED
           *  **/


          cv::Scalar high_value;
          cv::Scalar low_value ;
          cv::Mat hsv_img , mask_img , pre_marker_det ;
//          cv::Mat output_img =  Mat::zeros( input_img->size(), input_img->type() );

          cv::Mat3b hsv_pixel_image;
          std::vector<std::vector<cv::Point>> contours;

          switch (col){
          case(Color::BLUE): {
               high_value = cv::Scalar(10,255,255) ;
               low_value =  cv::Scalar(0,60,50) ;
               break;
          }
               
 
          case(Color::RED): {
               high_value = cv::Scalar(179,255,255) ;
               low_value =  cv::Scalar(160,100,20) ;
                break;
          }  
              
          
          case(Color::YELLOW) : {
                cv::Vec3b  yel (55 , 175,212);
                cv::Mat3b bgr_pixel_img(yel);
                cvtColor(bgr_pixel_img, hsv_pixel_image,COLOR_BGR2HSV); 
                cv::Scalar hsv_ = hsv_pixel_image.at<Vec3b>(0,0);
                high_value = cv::Scalar(hsv_[0]+20  , hsv_[1],hsv_[2]) ;
                low_value =  cv::Scalar(hsv_[0] - 10 , hsv_[1] - 150,hsv_[2]-150) ;
                break;
          
          }
                
          case(Color::BLACK) : {//black by default
               high_value = cv::Scalar(180, 255 , 30 ,0) ;
               low_value =  cv::Scalar(0, 0, 0 ,0) ;
               break;
          }           

          }
          input_img->copyTo(*output_img);
          cvtColor( *input_img, hsv_img, COLOR_BGR2HSV );
          cv::inRange(hsv_img , low_value , high_value , mask_img);
          cv::findContours(mask_img, contours , cv::RETR_EXTERNAL , cv::CHAIN_APPROX_SIMPLE);
      
          for (size_t i = 0; i<contours.size() ; i++){
               cv::Rect boundRect = cv::boundingRect(contours[i]);
               if( boundRect.area() > 500 && (boundRect.width >200 || boundRect.height>200)){
               rectangle(*output_img , boundRect.tl() , boundRect.br() , (0 , 0, 0) , 3) ;
               }               
            }
               
         // cv::imshow("bbox",output_img);
         // cv::waitKey(25);
          
         // return &output_img;
     }
     


   double Prosillica_Driver::marker_distance( float fx , float fy , std::vector<int> * image_size , cv::Size2f marker_size ){
        int focal_length = 12;
        float mx = fx / focal_length;
        float  pixel_per_mm= ( mx / 480) * (640) ;
        float obj_img_sensor = (marker_size.height / pixel_per_mm);
        float obj_dist = (5*focal_length)/ obj_img_sensor;
        return obj_dist;

     }



cv::Mat *  Prosillica_Driver::get_AUV_position( Eigen::Matrix3f & camera_mat ,Eigen::MatrixXf & dist_mat , std::vector<Point2f> & cont , cv::Point2f & tag_center , double depth){
//image point and object points 

    
    std::vector<Point3f> object_pts;
    std::vector<Point2f> img_pts;
    cv::Mat rvec(1,3,cv::DataType<float>::type);
    cv::Mat tvec(1,3,cv::DataType<float>::type);
    cv::Mat rotation_matrix(3,3,cv::DataType<float>::type);
    cv::Mat camera(3,3,cv::DataType<float>::type) ;
    cv::Mat dist_coeeffs ;
    eigen2cv(camera_mat , camera );

    eigen2cv(dist_mat , dist_coeeffs );
     

    
    object_pts.push_back(cv::Point3f(0 , 0, 0));
    object_pts.push_back(cv::Point3f(6.5 , 0 , 0));
    object_pts.push_back(cv::Point3f(6.5 , 6.5 , 0));
    object_pts.push_back(cv::Point3f(0 , 6.5 , 0));

    img_pts.push_back(cv::Point2f(292, 384));
    img_pts.push_back(cv::Point2f(343, 386));
    img_pts.push_back(cv::Point2f(342, 437));
    img_pts.push_back(cv::Point2f(292, 436));

    cv::solvePnP(object_pts , img_pts , camera , dist_coeeffs , rvec , tvec);
    cv::Rodrigues(rvec,rotation_matrix);
    cv::Mat uvPoint = (cv::Mat_<float>(3,1) << tag_center.x , tag_center.y, 1);
    
     cv::Mat camera_inv = camera.inv();
     
     cv::Mat pre_l_side = camera_inv * uvPoint; 

    cv::Mat leftSideMat  = rotation_matrix.inv() * pre_l_side;

    cv::Mat rightSideMat = rotation_matrix.inv() * tvec;

    float s = ((depth + rightSideMat.at<float>(2,0))/leftSideMat.at<float>(2,0)); 
    static  cv::Mat  P = rotation_matrix.inv() * ((s * camera_inv * uvPoint) - tvec);
    return & P;



}


void Prosillica_Driver::euler2Quaternion(double angle , double  distance_from_camera, cv::Point2f * img_coor , Eigen::Matrix3d &Camera_Mat)  
{  
    
    Eigen::Vector3d rotation(angle, 0, 0);
    Eigen::Vector3d translation(distance_from_camera, 0, 0);
    
    Eigen::Vector4d img_coordinates(img_coor->x, img_coor->y, 0 , 0);
    double ang = rotation.norm();
    Eigen::Vector3d axis = rotation.normalized();
    Eigen::Quaterniond q(Eigen::AngleAxisd(ang, axis));

    cout <<  "IS Euler2Quaternion Result:" <<endl;
    cout <<  " x  =" << q.x()  <<endl;
    Eigen::Matrix3d R = q.normalized().toRotationMatrix();
    cout <<  "IS Quaternion2RotationMatrix Result:" <<endl;
    cout <<  " R  =" << endl << R << endl<< endl;
    Eigen::MatrixXd K(R.rows(), R.cols()+translation.cols());
    K << R,translation;
    
 
  
} 